/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

//-----------------------------------------------------------
// .icc file for DataPool
//-----------------------------------------------------------
// includes:
#include <algorithm>
#include "GaudiKernel/System.h"


namespace SG {


/// Find the address of the function to clear an element,
/// or nullptr if it hasn't been set explicitly.
template <class VALUE, class CLEAR>
class DataPoolClearAddr
{
public:
  // Generic version --- a function that will call CLEAR::operator(),
  // casting the argument appropriately.
  static void callClear (SG::ArenaAllocatorBase::pointer p)
  {
    CLEAR::clear (reinterpret_cast<VALUE*> (p));
  }
  static SG::ArenaAllocatorBase::func_t* addr() { return callClear; }
};
template <class VALUE>
class DataPoolClearAddr<VALUE, SG::DataPoolNullClear<VALUE> >
{
public:
  // Specialized version of the default DataPoolNullClear.
  // Just return nullptr.
  static SG::ArenaAllocatorBase::func_t* addr() { return nullptr; }
};


} // namespace SG


#define DATAPOOL_T template <typename VALUE, typename CLEAR>
DATAPOOL_T
const typename DataPool<VALUE, CLEAR>::alloc_t::Params DataPool<VALUE, CLEAR>::s_params
  = DataPool<VALUE, CLEAR>::initParams();


DATAPOOL_T
typename DataPool<VALUE, CLEAR>::alloc_t::Params DataPool<VALUE, CLEAR>::initParams()
{
  typename alloc_t::Params params = alloc_t::initParams<VALUE> (s_minRefCount);
  params.clear = SG::DataPoolClearAddr<VALUE, CLEAR>::addr();
  return params;
}


//-----------------------------------------------------------

DATAPOOL_T
DataPool<VALUE, CLEAR>::DataPool(size_type n /*= 0*/)
  : m_handle (&s_params)
{
  if (n > 0)
    m_handle.reserve (std::max (n, s_minRefCount));
}

DATAPOOL_T
DataPool<VALUE, CLEAR>::DataPool(const EventContext& ctx,
                                 size_type n /*= 0*/)
  : m_handle (static_cast<SG::ArenaHeader*>(nullptr), ctx, &s_params)
{
  if (n > 0)
    m_handle.reserve (std::max (n, s_minRefCount));
}

DATAPOOL_T
DataPool<VALUE, CLEAR>::DataPool(SG::Arena* arena,
                                 size_type n /*= 0*/)
  : m_handle (arena, &s_params)
{
  if (n > 0)
    m_handle.reserve (std::max (n, s_minRefCount));
}

//-----------------------------------------------------------
/// release all elements in the pool.
DATAPOOL_T
void DataPool<VALUE, CLEAR>::reset()
{
  m_handle.reset();
}

/// free all memory in the pool.
DATAPOOL_T
void DataPool<VALUE, CLEAR>::erase()
{
  m_handle.erase();
}
//-----------------------------------------------------------
// reserve space for the pool
// allocated elements will not be deleted.

DATAPOOL_T
void DataPool<VALUE, CLEAR>::reserve(unsigned int size)
{
  m_handle.reserve (size);
}


DATAPOOL_T
void DataPool<VALUE, CLEAR>::prepareToAdd(unsigned int size)
{
  if (this->capacity() - this->allocated() < size) {
    this->reserve(this->allocated() + size);
  }
}

DATAPOOL_T
unsigned int DataPool<VALUE, CLEAR>::capacity()
{
  return m_handle.stats().elts.total;
}

DATAPOOL_T
unsigned int DataPool<VALUE, CLEAR>::allocated()
{
  return m_handle.stats().elts.inuse;
}


//-----------------------------------------------------------
/// begin iterators over pool
DATAPOOL_T
typename DataPool<VALUE, CLEAR>::iterator DataPool<VALUE, CLEAR>::begin()
{
  return iterator (m_handle.begin());
}

DATAPOOL_T
typename DataPool<VALUE, CLEAR>::const_iterator DataPool<VALUE, CLEAR>::begin() const
{
  return const_Iterator (m_handle.begin());
}

//-----------------------------------------------------------
/// the end() method will allow looping over only valid elements
/// and not over ALL elements of the pool

DATAPOOL_T
typename DataPool<VALUE, CLEAR>::iterator DataPool<VALUE, CLEAR>::end()
{
  return iterator (m_handle.end());
}

DATAPOOL_T
typename DataPool<VALUE, CLEAR>::const_iterator DataPool<VALUE, CLEAR>::end() const {
  return const_iterator (m_handle.end());
}

//-----------------------------------------------------------
/// typename of pool
DATAPOOL_T
const std::string& DataPool<VALUE, CLEAR>::typeName() {
  static std::string name = System::typeinfoName (typeid (VALUE));
  return name;
}

//-----------------------------------------------------------
/// obtain the next available element in pool by pointer
/// pool is resized if reached its limit
DATAPOOL_T
inline
typename DataPool<VALUE, CLEAR>::pointer DataPool<VALUE, CLEAR>::nextElementPtr()
{
  return m_handle.allocate();
}


#undef DATAPOOL_T
