# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

AntiKt10TruthWZJetsCPContent = [
"AntiKt10TruthWZJets",
"AntiKt10TruthWZJetsAux.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor"
]

